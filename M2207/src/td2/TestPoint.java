package td2;

public class TestPoint {

	public static void main(String[] args) {
		//D�clarations
		Point p;
		Point p2;
		
		//Instanciation
		p = new Point(2,7);
		p2 = new Point();
		
		System.out.println("x = " + p.getX());
		
		p.setX(3);
		
		System.out.println("x = " + p.getX());
		
		//Exercice 4
		/*System.out.println(p.x); impossible car en priv� pour 
		 cela il faut modifier les accesseur*/
		
		p.seDecrire();
		p2.seDecrire();
		p.resetCoordonnees();
		p2.deplacer(5,3);
		p2.seDecrire();
	}

}
