package td3;

public class Animal {

	//Methode

	public String affiche() {
		return "Je suis un animal";
	}
	
	public String cri() {
		return "...";
	}
	
	public final String origine() {
		return "La classe Animal est la m�re de toutes les classes";
	}
}
