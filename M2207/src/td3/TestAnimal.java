package td3;

public class TestAnimal {

	public static void main(String[] args) {
		//Declaration
		Animal animal;
		Chien chien;
		Chat chat;

		//Instanciation
		animal = new Animal();
		chien = new Chien();
		chat = new Chat();

		//Test toString
		String message = animal.toString();
		System.out.println(message);

		//Test animal affiche() et cri()
		System.out.println(animal.affiche());
		System.out.println(animal.cri());

		//Test chien affiche() et cri()
		System.out.println(chien.affiche());
		System.out.println(chien.cri());

		//Test originie()
		System.out.println(animal.origine());
		System.out.println(chien.origine());
		
		//Test chat affiche()
		System.out.println(chat.affiche());
		
		//Test exercice 3.4	
		System.out.println(animal.cri());
		System.out.println(chien.cri());
		System.out.println(chat.cri());
		System.out.println(chat.miauler());
	}

}
