package tp1;

public class Client {
	//Attribut
	private String nom, prenom;
	private Compte compteCourant;

	//Constructeur
	public Client(String n, String p, Compte compte) {
		nom = n;
		prenom = p;
		compteCourant = compte;
	}

	//Accesseur
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public double getSolde() {
		return compteCourant.getSolde();
	}
	public void afficherSolde() {
		System.out.println(compteCourant.getSolde());
	}
}
