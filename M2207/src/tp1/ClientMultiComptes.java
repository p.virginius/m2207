package tp1;

public class ClientMultiComptes {
	//Attribut
	private String nom, prenom;
	private Compte[] tabcomptes = new Compte[10];
	private int nbComptes = 1;
	
	//Constructeur
	public ClientMultiComptes(String nom, String prenom, Compte compte) {
		this.nom = nom;
		this.prenom = prenom;
		tabcomptes[0] = compte;
	}
	
	//Methode
	public void ajouterCompte(Compte c) {
		tabcomptes[nbComptes] = c;
		nbComptes++;
	}
	public double getSolde() {
		double somme = 0;
		for (int i = 0; i < nbComptes; i++) {
			somme = somme + tabcomptes[i].getSolde();
		}
		return somme;
	}
	public void afficherEtatClient() {
		System.out.println("Client: " + "nom" + "prenom");
		for(int i = 0; i < nbComptes; i++) {
			System.out.println("Compte: " + " " + i + " - " + "Solde: " + tabcomptes[i].getSolde());
		}
		System.out.println("Solde Client: " + this.getSolde());
	}
}
