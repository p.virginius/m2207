package tp1;

public class Compte {
	//Attribut
	private int numero;
	private double solde, decouvert;

	//Constructeur
	public Compte(int numero) {
		solde = 0;
		decouvert = 0;
	}

	//Accesseurs
	public void setDecouvert(double montant) {
		decouvert = montant;
	}
	public double getDecouvert() {
		return decouvert;
	}

	public int getNumero() {
		return numero;
	}
	public double getSolde(){
		return solde;
	}

	//Methode
	public void afficherSolde() {
		System.out.println("Votre solde est de " + solde);
	}
	public void depot(double montant) {
		solde = solde + montant; 
	}
	public String retrait(double montant) {
		String message; 
		if (montant > solde + decouvert) {
			message = "Retrait refus�";
		}
		else {
			solde = solde - montant;
			message = "Retrait effectu�";
		}
		return message;
	}
}
