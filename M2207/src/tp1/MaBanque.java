package tp1;

public class MaBanque {

	public static void main(String[] args) {
		//D�claration
		Compte compte1;
		Compte compte2;
		Client client;

		//Instanciation
		compte1 = new Compte(2);
		compte2 = new Compte(2);
		client = new Client("VIRGINIUS", "Patrice, ", compte2);

		/*Test d�couvert solde retrait compte 1
		System.out.println("D�couvert: " + compte1.getDecouvert());
		compte1.setDecouvert(100);
		System.out.println("D�couvert: " + compte1.getDecouvert());
		System.out.println("Solde: " + compte1.getSolde());
		compte1.afficherSolde();
		compte1.depot(100);
		System.out.println("Solde: " + compte1.getSolde());
		compte1.afficherSolde();
		System.out.println(compte1.retrait(50));
		compte1.afficherSolde();
		System.out.println("D�couvert: " + compte1.getDecouvert());
		 */

		/*Test compte 2
		compte2.depot(1000);
		System.out.println("Solde: " + compte2.getSolde());
		System.out.println(compte2.retrait(600));
		compte2.afficherSolde();
		System.out.println(compte2.retrait(700));
		compte2.afficherSolde();
		compte2.setDecouvert(500);
		System.out.println("D�couvert: " + compte2.getDecouvert());
		System.out.println(compte2.retrait(700));
		compte2.afficherSolde();

		//Client 3
		System.out.println("Le Client 1: " + client.getNom() + " " + client.getPrenom());
		client.afficherSolde();
		*/
		
		/*Exo4
		Declaration*/
		ClientMultiComptes client10;
		Compte compte10;
		Compte compte20;
		
		//Instanciation
		compte10 = new Compte(10);
		compte20 = new Compte(20);
		client10 = new ClientMultiComptes("bob", "payet", compte10);
		
		//Affichage
		compte10.depot(1000);
		System.out.println("Solde:" + client10.getSolde());
		client10.ajouterCompte(compte20);
		compte20.depot(2500);
		System.out.println("Solde:" + client10.getSolde());
		client10.afficherEtatClient();
	}

}
