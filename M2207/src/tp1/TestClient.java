package tp1;

public class TestClient {

	public static void main(String[] args) {
		//D�claration
		Compte c2;
		Client client;
		//Exo 4
		ClientMultiComptes client5;
		Compte c3;

		//Instanciation
		c2 = new Compte(3);
		client = new Client("VIRGINIUS", "Patrice, ", c2);
		//Exo 4
		c3 = new Compte(3);
		client5 = new ClientMultiComptes("TCF", "Louis", c3);

		//Test nom et pr�nom
		System.out.println("Le Client 1: " + client.getNom() + " " + client.getPrenom());
		//Test getsSolde et afficherSolde
		System.out.println("Solde:"+client.getSolde());
		client.afficherSolde();
		
		//Exo 4
		System.out.println("Solde:"+client5.getSolde());
	}
}

