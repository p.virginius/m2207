package tp2;

public class Cercle extends Forme {
	//Attribut
	private double rayon;

	//Constructeur
	public Cercle(){
		super();
		this.rayon = 1.0;
	}
	public Cercle(double r) {
		this.rayon = r;
	}
	public Cercle(double r, String couleur, boolean coloriage) {
		super(couleur, coloriage);
		rayon = r;
	}

	//Accesseur
	public double getRayon() {
		return this.rayon;
	}
	public void setRayon(double r) {
		this.rayon = r;
	}

	//Methode
	public String seDecrire() {
		return "Un Cercle de rayon " + this.rayon + " est issue d'une " + super.seDecrire();
	}
	public double calculerAire() {
		return Math.PI*rayon*rayon;
	}
	public double calculerPerimetre() {
		return Math.PI*2*rayon;
	}
}
