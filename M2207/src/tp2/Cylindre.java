package tp2;

public class Cylindre extends Cercle {
	//Attribut
	private double hauteur;

	//Constructeur
	public Cylindre() {
		hauteur = 1.0;
	}
	public Cylindre(double r, String couleur, boolean coloriage) {
		super(r, couleur, coloriage);
	}

	//Accesseur
	public double getHauteur() {
		return hauteur;
	}
	public void setCylindre(double h) {
		hauteur = h;
	}

	//Methode
	public String seDecrire() {
		return "Un Cercle de hauteur " + hauteur + " est issue d'une " + super.seDecrire();
	}
	public double calculerVolume() {
		return Math.PI*Math.pow(super.getRayon(), 2)*hauteur;
	}

}
