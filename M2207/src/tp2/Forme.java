package tp2;

public class Forme {
	//Attribut
	private String couleur;
	private boolean coloriage;
	private static int nombre_objet;
	
	//Constructeur
	public Forme() {
		couleur = "orange";
		coloriage = true;
		nombre_objet = nombre_objet + 1;
	}
	public Forme(String c, boolean r) {
		couleur = c;
		coloriage = r;
		nombre_objet = nombre_objet + 1;
	}
	
	//Accesseur
	public String getCouleur() {
		return couleur;
	}
	public void setCouleur(String c) {
		couleur = c;
	}
	public boolean isColoriage() {
		return coloriage;
	}
	public void setColoriage(boolean b) {
		coloriage = b;
	}
	
	//Methode
	public String seDecrire() {
		return "Une Forme de couleur " + getCouleur() + " et de coloriage " + isColoriage();
	}
}
