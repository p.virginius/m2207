package tp2;

public class TestForme {

	public static void main(String[] args) {
		//Declaration
		Forme f1;
		Forme f2;
		
		//Instanciation
		f1 = new Forme();
		f2 = new Forme("vert", false);
		
		//Affichage f1 et f2
		System.out.println(f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println(f2.getCouleur() + " - " + f2.isColoriage());
		f1.setCouleur("rouge");
		f1.setColoriage(false);
		System.out.println(f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println(f1.seDecrire());
		System.out.println(f2.seDecrire());
		
		//Cercle 1
		//Declaration
		Cercle c1;
		Cercle c2;
		
		//Instanciation
		c1 = new Cercle();
		c2 =new Cercle();
		
		//Affichage cercle 1
		System.out.println(c1.seDecrire());
		c2.setRayon(2.5);
		System.out.println(c2.seDecrire());
		
		//	Cercle c3
		//Declaration
		Cercle c3;
		
		//Instanciation
		c3 = new Cercle(3.2, "jaune", false);
		
		//Affichage cercle 3
		System.out.println(c3.seDecrire());
		
		//Affichage cercle 2 et 3
		System.out.println(c2.calculerAire());
		System.out.println(c3.calculerAire());
		System.out.println(c2.calculerPerimetre());
		System.out.println(c3.calculerPerimetre());
		
		//Cylindre
		//Declaration
		Cylindre cy1;
		Cylindre cy2;
		
		//Instanciation
		cy1 = new Cylindre();
		cy2 = new Cylindre();
		
		//Affichage cy1
		System.out.println(cy1.seDecrire());
		
		//Affichage cy2 et volume de cy1 et 2
		cy2.setCylindre(4.2);
		cy2.setRayon(1.3);
		cy2.setCouleur("bleu");
		cy2.setColoriage(true);
		System.out.println(cy2.seDecrire());
		System.out.println(cy1.calculerVolume());
		System.out.println(cy2.calculerVolume());
	}

}
