package tp3;

public class Brigand extends Humain {
	//Attribut
	private String look;
	private int nombre_de_dame, recompense;
	private boolean en_prison;

	//Constructeur
	public Brigand(String nom) {
		super(nom);
		boisson_favorite = "cognac";
		en_prison = false;
		nombre_de_dame = 0;
		recompense = 100;
		look = "mechant";
	}

	//Accesseurs

	//Methode 
	
	//Methode quelEstTonNom et getRecompense
	public int getRecompense() {
		return recompense;
	}
	public String quelEstTonNom() {
		return nom + " le " + look;
	}
	
	//Methode sePresenter
	public void sePresenter(){
		super.sePresenter();
		parler("J'ai l'air M�chant et j'ai enlev� " + nombre_de_dame + " dames.");
		parler("Ma t�te est mise � prix " + recompense + "$ !");
	}
	
	//Methode enleve
	public void enleve(Dame dame) {
		nombre_de_dame = nombre_de_dame + 1;
		recompense = recompense + 100;
		dame.priseEnOtage();
		parler("Ah ah ! " + dame.quelEstTonNom() + ", tu es ma prisonni�re");
	}
	public void emprisonner(Sherif sherif) {
		parler("Damned, je suis fait ! " + sherif.quelEstTonNom() + ", tu m'as eu!");
	}
}
