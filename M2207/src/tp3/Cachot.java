package tp3;

public class Cachot {
	//Attribut
	private String nom;
	private Humain[] tabPrisonnier;
	int nbPrisonnier = 0;

	//Constructeur
	public Cachot(String nom) {
		this.nom = nom;
		tabPrisonnier = new Humain[10];
	}

	//Methode 
	//Methode mettre en cellule
	public void mettreEnCellule(Humain humain) {
		tabPrisonnier[nbPrisonnier] = humain;
		System.out.println(humain.quelEstTonNom() + " est mis dans la cellule n�" + nbPrisonnier);
		nbPrisonnier++;
	}
	//Methode sortir de la cellule
	public void sortirDeCellule(Humain humain) {
		for (int i = 0; i<tabPrisonnier.length; i++) {
			if(tabPrisonnier[i]==humain) {
				tabPrisonnier[i] = null;
			}
		}
		nbPrisonnier--;
		System.out.println(humain.quelEstTonNom() + " est sortie de sa cellule");		
	}
	//Methode methode pour compter les prisonnier
	public void compterLesPrisonniers() {
		System.out.println("Il y a " + nbPrisonnier + " Prisonnier dans le cachot " + nom);

	}
}
