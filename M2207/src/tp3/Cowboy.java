package tp3;

public class Cowboy extends Humain {
	//Attribut
	private int success;
	private String caracteristique;
	
	//Constructeur 
	public Cowboy(String nom) {
		super(nom);
		boisson_favorite = "Whiskey";
		success = 0;
		caracteristique = "vaillant";
	}
	
	//Accesseur
	
	//Methode
	//Methode qui permet de donner le nom
	public String quelEstTonNom() {
		return nom + " le " + caracteristique;
	}
	
	//Methode qui permet au cowbow de tirer
	public void tire(Brigand brigand) {
		parler(quelEstTonNom() + " tire sur " + brigand.quelEstTonNom() + ". PAN !");
		parler("Prend �a, voyou !");
	}
	
	//Methode qui permet au cowboy de liberee une dame
	public void libere(Dame dame) {
		dame.estLiberee();
		success = success + 10;
	}
	
}
