package tp3;

public class Dame extends Humain{
	//Attribut 
	private boolean libre;

	//Constructeur
	public Dame(String nom) {
		super(nom);
		boisson_favorite = "Martini";
		libre = true;
	}

	//Accesseur

	//Methode
	//Methode priseEnOtage
	public void priseEnOtage() {
		libre = false;
		parler("Au secour");
	}
	
	//Methode estLiberee
	public void estLiberee() {
		libre = true;
		parler("Merci Cowboy");
	}
	
	// Methode quelEstTonNom et sePresenter
	public String quelEstTonNom() {
		return "Miss " + nom;
	}
	//Methode qui permet de se presenter
	public void sePresenter() {
		super.sePresenter();
		if (libre = false) {
			parler("Actuellement, je suis kidnapp�e");
		}
		else {
			libre = true;
			parler("Actuellement, je suis libre");
		}
	}
}
