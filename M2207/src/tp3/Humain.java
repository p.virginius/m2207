package tp3;

public class Humain {
	//Attribut
	protected String nom, boisson_favorite;

	//Constructeur
	public Humain(String nom) {
		this.nom = nom;
		boisson_favorite = "lait";
	}

	//Accesseur

	//Methode
	public String quelEstTonNom() {
		return nom;
	}
	public String quelleEstTaBoisson() {
		return boisson_favorite;
	}

	//Methode parler et sePresenter
	public void parler(String texte) {
		System.out.println("(" + nom + ") " + "- " + texte);
	}
	public void sePresenter() {
		parler("Bonjour, je suis " + quelEstTonNom() + " et ma boisson pr�f�r�e est le " + quelleEstTaBoisson());
	}

	//Methode boire
	public void boire() {
		parler("Ah! un bon verre de " + quelleEstTaBoisson() + " ! GLOUPS !");
	}


}
