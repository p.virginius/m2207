package tp3;

public class Prison {
	//Attribut
	private String nom;
	private Brigand[] tabBrigand;
	int nbBrigand = 0;

	//Constructeur
	public Prison(String nom) {
		this.nom = nom;
		tabBrigand = new Brigand[10];
	}

	//Methode 
	//Methode pour mettre en cellule
	public void mettreEnCellule(Brigand brigand) {
		tabBrigand[nbBrigand] = brigand;
		System.out.println(brigand.quelEstTonNom() + " est mis dans la cellule n�" + nbBrigand);
		nbBrigand++;
	}
	//Methode pour sortir de la cellule
	public void sortirDeCellule(Brigand brigand) {
		for (int i = 0; i<tabBrigand.length; i++) {
			if(tabBrigand[i]==brigand) {
				tabBrigand[i] = null;
			}
		}
		nbBrigand--;
		System.out.println(brigand.quelEstTonNom() + " est sortie de sa cellule");		
	}
	//Methode pour compter les prisonniers
	public void compterLesPrisonniers() {
		System.out.println("Il y a " + nbBrigand + " Brigands dans la prison " + nom);

	}

}
