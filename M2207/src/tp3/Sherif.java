package tp3;

public class Sherif extends Cowboy {
	//Attribut
	private int nombre_de_brigand;
	private String commence;
	
	//Constructeur
	public Sherif(String nom) {
		super(nom);
		commence = "Sherif";
		nombre_de_brigand = 0;
	}
	
	//Accesseur
	
	//Methode
	//Methode qui permet au Sherif de se donner son nom
	public String quelEstTonNom() {
		return nom + " le " + commence;
	}
	//Methode qui permet de se presenter
	public void sePresenter() {
		super.sePresenter();
		parler("J'ai d�j� arret� " + nombre_de_brigand + " Brigand");
	}
	//Methode qui permet de coffrer les brigand et modifier le nombre de brigand capturer.
	public void coffrer(Brigand brigand) {
		parler("Au nom de la loi, je vous arr�te, " + brigand.quelEstTonNom());
		nombre_de_brigand = nombre_de_brigand +1;
	}
}
