package tp4;

public class Combat {

	public static void main(String[] args) {
		//Declaration de pokemon
		Pokemon pokemon;
		Pokemon pokemon1;
		String vainqueur = null;

		//Instanciation de pokemon
		pokemon = new Pokemon("Pikatchoin");
		pokemon1 = new Pokemon("BullBitruc");

		/*Test COMBAT
		System.out.println("Exercice 2");
		System.out.println(" ");
		pokemon.sePresenter();
		pokemon1.sePresenter();
		System.out.println("- - - - - - - - - - - - - - - - - - ");
		int round = 0;
		while(pokemon.isAlive() && pokemon1.isAlive()) {
			pokemon.attaquer(pokemon1);
			pokemon1.attaquer(pokemon);
			round = round +1;
			System.out.println("Round " + round + " " + pokemon.getNom() + " : " + pokemon.getEnergie() + " " + pokemon1.getNom() + " : " + pokemon1.getEnergie());
		}

		//Affiche vainqueur
		if(pokemon.isAlive()) {
			vainqueur = pokemon.getNom();
		}
		else {
			if(pokemon1.isAlive()) {
				vainqueur = pokemon1.getNom();			
			}
			else {
				vainqueur = "Aucun pok�mon ne ";
			}
		}
		System.out.println("- - - - - - - - - - - - - - - - - - ");
		System.out.println(vainqueur + " gagne en " + round + " rounds"); */

		//Test COMBAT
		System.out.println("Exercice 3");
		System.out.println(" ");
		pokemon.sePresenter();
		pokemon1.sePresenter();
		System.out.println("- - - - - - - - - - - - - - - - - - ");
		int round = 0;
		while(pokemon.isAlive() && pokemon1.isAlive()) {
			pokemon.attaquer(pokemon1);
			pokemon1.attaquer(pokemon);
			round = round +1;
			System.out.println("Round " + round + " " + pokemon.getNom() + " : (en)" + pokemon.getEnergie() + " (atk)" + pokemon.getPuissance() + " - " + pokemon1.getNom() + " : (en)" + pokemon1.getEnergie() + " (atk)" + pokemon1.getPuissance());
		}

		//Affiche vainqueur
		if(pokemon.isAlive()) {
			vainqueur = pokemon.getNom();
		}
		else {
			if(pokemon1.isAlive()) {
				vainqueur = pokemon1.getNom();			
			}
			else {
				vainqueur = "Aucun pok�mon ne ";
			}
		}
		System.out.println("- - - - - - - - - - - - - - - - - - ");
		System.out.println(vainqueur + " gagne en " + round + " rounds");
	}
}
