package tp4;

import java.util.Scanner;

public class CombatAvecJoueur {
	private static Scanner sc;

	public static void main(String[] args) {
		sc = new Scanner(System.in);

		//Declaration
		Pokemon pokemon;
		Pokemon pokemon1;
		String vainqueur = null;

		//Instanciation et Test Combat
		System.out.println("Saisir le nom du premier combattant: ");
		String nom1 = sc.nextLine();
		pokemon = new Pokemon(nom1);
		System.out.println("Saisir le nom du deuxieme combattant: ");
		String nom2 = sc.nextLine();
		pokemon1 = new Pokemon(nom2);

		System.out.println("- - - - - - - - - - - - - - - - - - - -");
		int round = 0;
		while(pokemon.isAlive() && pokemon1.isAlive()) {
			System.out.println("Round " + round);
			round = round + 1;
			System.out.println("Etat des combattants: " + pokemon.getNom() + " (en)" + pokemon.getEnergie() + " (atk)" + pokemon.getPuissance() + " / " + pokemon1.getNom() + " (en)" + pokemon1.getEnergie() + " (atk)" + pokemon1.getPuissance());
			System.out.println(pokemon.getNom() + ": Attaquer(0) ou Manger(1)");
			int i = sc.nextInt();
			System.out.println(pokemon1.getNom() + ": Attaquer(0) ou Manger(1)");
			int j = sc.nextInt();

			switch (i) {
			case 0: pokemon.attaquer(pokemon1);
			break;
			case 1: pokemon.manger();
			break;
			default: pokemon.attaquer(pokemon1);
			break;
			}

			switch (j) {
			case 0: pokemon1.attaquer(pokemon);
			break;
			case 1: pokemon1.manger();
			break;
			default: pokemon1.attaquer(pokemon);
			break;
			}
			//Affiche vainqueur
			if(pokemon.isAlive()) {
				vainqueur = pokemon.getNom();
			}
			else {
				if(pokemon1.isAlive()) {
					vainqueur = pokemon1.getNom();			
				}
				else {
					vainqueur = "Aucun pok�mon ne ";
				}
			}
			System.out.println("Etat des combattants: " + pokemon.getNom() + " (en)" + pokemon.getEnergie() + " (atk)" + pokemon.getPuissance() + " / " + pokemon1.getNom() + " (en)" + pokemon1.getEnergie() + " (atk)" + pokemon1.getPuissance());
			System.out.println("- - - - - - - - - - - - - - - - - - - -");
		}
		System.out.println(vainqueur + " gagne en " + round + " rounds");
	}

}
