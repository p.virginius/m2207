package tp5;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridLayout;

public class MonAppliGraphique extends JFrame {
	//Attribut
	JButton b0, b1,b2, b3, b4;
	JLabel monLabel;
	JTextField monTextField;

	//Constructeur
	//Cr�ation de la fen�tre
	public MonAppliGraphique () {
		super();
		this.setTitle("Ma premi�re application");
		this.setSize(400,200);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr�e!");

		//Cr�ation du bouton
		b0 = new JButton("BOUTON 0");
		b1 = new JButton("BOUTON 1");
		b2 = new JButton("BOUTON 2");
		b3 = new JButton("BOUTON 3");
		b4 = new JButton("BOUTON 4");

		//Ajout de Container
		Container panneau = getContentPane();

		//Ajout de setLayout
		panneau.setLayout(new GridLayout(3, 2));
		
		//Ajout de la composante graphique de Container
		panneau.add(b0, BorderLayout.NORTH);
		panneau.add(b1, BorderLayout.WEST);
		panneau.add(b2);
		panneau.add(b3, BorderLayout.EAST);
		panneau.add(b4, BorderLayout.SOUTH);

		/* //Ajout de JLabel
		monLabel = new JLabel("Je suis JLabel");
		panneau.add(monLabel);

		//Ajout de JTextField
		monTextField = new JTextField("Je suis un JTextField");
		panneau.add(monTextField); */

		//setVisible
		this.setVisible(true);// Toujours � la fin du constructeur
	}

	//Accesseur


	//Methode
	//Methode main de MonAppli
	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique () ;
	}

}
