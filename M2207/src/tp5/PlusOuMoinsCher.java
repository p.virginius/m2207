package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PlusOuMoinsCher extends JFrame implements ActionListener {
	//Attributs
	JButton b;
	JLabel monLabel, proposition;
	JTextField entrer;
	int a = 0;

	//Constructeur
	//Cr�ation de la fen�tre
	public PlusOuMoinsCher() {
		super();
		this.setTitle("Plus cher ou moins cher");
		this.setSize(350,150);
		this.setLocation(350,150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr�e!");

		//Ajout de Container
		Container panneau = getContentPane();

		//Ajout de setLayout
		panneau.setLayout(new GridLayout(2, 2));

		//Ajout de JLabel et JTextField et bouton
		proposition = new JLabel("Votre proposition:");
		panneau.add(proposition);
		entrer = new JTextField("Entrez votre proposition.");
		panneau.add(entrer);
		b = new JButton("V�rifie!");
		panneau.add(b);
		monLabel = new JLabel("La r�ponse");
		panneau.add(monLabel);
		

		// Ajout de la m�thode � ex�cuter lors d�une action.
		b.addActionListener(this);

		//setVisible
		this.setVisible(true);// Toujours � la fin du constructeur
	}

	//Methode
	//Methode main de PlusOuMoinsCher
	public static void main(String[] args) {
		PlusOuMoinsCher app = new PlusOuMoinsCher() ;
	}
	//Methode executer lors d'une action
	public void actionPerformed(ActionEvent e) {
		System.out.println();
		a = a+1;
		monLabel.setText("Vous avez cliqu� " + a + " fois");

	}
}
