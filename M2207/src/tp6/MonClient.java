package tp6;

import java.net.Socket;
import java.io.PrintWriter;

public class MonClient {

	public static void main(String[] args) {
		//Declaration
		Socket monSocket;
		PrintWriter monPrintWriter;
		
		try {
			monSocket = new Socket("localhost", 8888);
			System.out.println("Client: " + monSocket);
			monPrintWriter = new PrintWriter(monSocket.getOutputStream());
			System.out.println("Envoi du message : Hello World");
			monPrintWriter.println("Hello World");
			monPrintWriter.flush();// permet de forcer l'envoie de mani�re imm�diate
			monSocket.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

}
