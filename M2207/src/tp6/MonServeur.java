package tp6;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MonServeur {

	public static void main(String[] args) {
		//Declaration
		ServerSocket monServerSocket;
		Socket monSocketClient;
		BufferedReader monBufferedReader;
		
		try {
			monServerSocket = new ServerSocket(8888);
			System.out.println("ServerSocket: " + monServerSocket);
			monSocketClient = monServerSocket.accept();
			System.out.println("Le client s'est connect�");
			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream()));
			String message = monBufferedReader.readLine();
			System.out.println("Message : " + message);
			monServerSocket.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}

}
