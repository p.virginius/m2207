package tp6;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.io.PrintWriter;

public class PanneauClient extends JFrame implements ActionListener {
	//Atribut
	JButton b1;
	JTextField text;
	Socket monSocket;
	PrintWriter monPrintWriter;
	

	//Constructeur
	//Cr�ation de la fen�tre
	public PanneauClient() {
		super();
		this.setTitle("Client - Panneau d'affichage");
		this.setSize(250,120);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr�e!");
		
		Container panneau = getContentPane();
		panneau.setLayout(new FlowLayout());
		
		text = new JTextField("                                                       ");
		panneau.add(text);
		
		b1 = new JButton("Envoyer");
		b1.addActionListener(this);
		panneau.add(b1, BorderLayout.SOUTH);
		
		try {
			monSocket = new Socket("localhost", 8888);
			System.out.println("Client: " + monSocket);
			monPrintWriter = new PrintWriter(monSocket.getOutputStream());
			monPrintWriter.flush();// permet de forcer l'envoie de mani�re imm�diate
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		this.setVisible(true);// Toujours � la fin du constructeur
		
	}
	
	//Methode emettre
	public void emettre(String m) {
		System.out.println("Envoie du message: " + m);
		try {
			monPrintWriter.println(m);
			monPrintWriter.flush();
		}
		catch(Exception e) {
			System.out.println("Erreur");
		}
		
	}
	
	//Methode
	public static void main(String[] args) {
		new PanneauClient();

	}
	public void actionPerformed(ActionEvent e) {
		emettre(text.getText());
		
	}

}
