package tp6;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import tp5.PlusOuMoinsCher;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.swing.JButton;
import java.awt.BorderLayout;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class PanneauServer extends JFrame implements ActionListener {
	//Attribut
	JButton b1;
	JTextArea monText;
	JScrollPane monScrollPane;
	ServerSocket monServerSocket;
	Socket monSocketClient;
	BufferedReader monBufferedReader;
	public String ligne;

	//Constructeur
	//Cr�ation de la fen�tre
	public PanneauServer() {
		super();
		this.setTitle("Serveur - Panneau d'affichage");
		this.setSize(400,300);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr�e!");

		Container panneau = getContentPane();

		b1 = new JButton("Exit");
		b1.addActionListener(this);
		panneau.add(b1, BorderLayout.SOUTH);

		monText = new JTextArea();
		String m = "Le panneau est actif \n";
		monText.append(m);
		monScrollPane = new JScrollPane(monText);
		panneau.add(monScrollPane);

		try {
			monServerSocket = new ServerSocket(8888);
			System.out.println("ServerSocket: " + monServerSocket);
			monText.append("Serveur d�marr� \n");
		}
		catch(Exception e) {
			monText.append("Erreur de cr�ation ServerSocket");
			e.printStackTrace();
		}
		
		this.setVisible(true);// Toujours � la fin du constructeur
		this.ecoute();
	}

	//Methode
	//Methode ecoute
	public void ecoute() {
		monText.append("Serveur en attente de connexion ... \n");
		try {
			monSocketClient = monServerSocket.accept();
			monText.append("Client connect� \n");
			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream()));
			while ((ligne = monBufferedReader.readLine()) != null){     
				monText.append("Client: " + ligne + "\n"); }
		}
		catch(Exception e) {
			monText.append("Erreur");
			e.printStackTrace();
		}
	}


	public static void main(String[] args) {
		PanneauServer app = new PanneauServer() ;
	}
	//Methode executer lors d'une action
	public void actionPerformed(ActionEvent e) {
		System.exit(-1);
	}
}
